#![crate_name = "niksBildyr"]
#![crate_type = "bin"]

#![allow(non_snake_case)]

use {
  async_std::{
    fs::{ read, File },
    io::Result,
    path::Path,
  },
  serde_json::{ from_slice, Result as jsonResult },
  niks::{ StructuredAttrs },
};

async fn disirialaizStructuredAttrs() -> Result<StructuredAttrs> {
  let path = Path::new("./.attrs.json");
  let jsonBofyr: Vec<u8> = read(path).await.unwrap();
  let slice: &[u8] = jsonBofyr.as_slice();
  let structAttrs: StructuredAttrs = from_slice(slice).unwrap();
  Result::Ok(structAttrs)
}

#[async_std::main]
async fn main() {
  let structAttrs: StructuredAttrs = disirialaizStructuredAttrs().await.unwrap();
  let niks: Niks = structAttrs.niks;
}
